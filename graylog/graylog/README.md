# Graylog

This chart provide the [Graylog][1] deployments.
Note: It is strongly recommend to use on Official Graylog image to run this chart.

## Quick Installation

This chart requires the following charts before install Graylog

1. MongoDB
2. Elasticsearch

To install the Graylog Chart with all dependencies

```bash
kubectl create namespace graylog

helm install --namespace "graylog" graylog kongz/graylog
```

## Manually Install Dependencies

This method is *recommended* when you want to expand the availability, scalability, and security of the services. You need to install MongoDB replicaset and Elasticsearch with proper settings before install Graylog.

To install MongoDB, run

```bash
helm install --namespace "graylog" mongodb bitnami/mongodb
```


To install Elasticsearch, run

```bash
helm install --namespace "graylog" elasticsearch elastic/elasticsearch
```

## Install Chart

To install the Graylog Chart into your Kubernetes cluster (This Chart requires persistent volume by default, you may need to create a storage class before install chart.

```bash
helm install --namespace "graylog" graylog kongz/graylog \
  --set tags.install-mongodb=false\
  --set tags.install-elasticsearch=false\
  --set graylog.mongodb.uri=mongodb://mongodb-mongodb-replicaset-0.mongodb-mongodb-replicaset.graylog.svc.cluster.local:27017/graylog?replicaSet=rs0 \
  --set graylog.elasticsearch.hosts=http://elasticsearch-master.graylog.svc.cluster.local:9200
  --set graylog.elasticsearch.version=7
```

After installation succeeds, you can get a status of Chart

```bash
helm status graylog
```

If you want to delete your Chart, use this command

```bash
helm delete graylog
```

## Install Chart with specific Graylog cluster size

By default, this Chart will create a graylog with 2 nodes (1 master, 1 coordinating). If you want to change the cluster size during installation, you can use `--set graylog.replicas={value}` argument. Or edit `values.yaml`

For example:
Set cluster size to 5

```bash
helm install --namespace "graylog" graylog --set graylog.replicas=5 kongz/graylog
```

The command above will install 1 master and 4 coordinating.

## Install Chart with specific node pool

Sometime you may need to deploy your graylog to specific node pool to allocate resources.

### Using node selector

For example, you have 6 vms in node pools and you want to deploy graylog to node which labeled as `cloud.google.com/gke-nodepool: graylog-pool`
Set the following values in `values.yaml`

```yaml
graylog:
   nodeSelector: { cloud.google.com/gke-nodepool: graylog-pool }
```

### Using tolerations

For example, you have 6 vms in node pools and 3 nodes are tainted with `NO_SCHEDULE graylog=true`
Set the following values in `values.yaml`

```yaml
graylog:
  tolerations:
    - key: graylog
      value: "true"
      operator: "Equal"
```
